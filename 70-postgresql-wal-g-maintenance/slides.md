%title: Infra Cloud Infomaniak
%author: xavki


██████╗  ██████╗ ███████╗████████╗ ██████╗ ██████╗ ███████╗███████╗ ██████╗ ██╗     
██╔══██╗██╔═══██╗██╔════╝╚══██╔══╝██╔════╝ ██╔══██╗██╔════╝██╔════╝██╔═══██╗██║     
██████╔╝██║   ██║███████╗   ██║   ██║  ███╗██████╔╝█████╗  ███████╗██║   ██║██║     
██╔═══╝ ██║   ██║╚════██║   ██║   ██║   ██║██╔══██╗██╔══╝  ╚════██║██║▄▄ ██║██║     
██║     ╚██████╔╝███████║   ██║   ╚██████╔╝██║  ██║███████╗███████║╚██████╔╝███████╗
╚═╝      ╚═════╝ ╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝ ╚══▀▀═╝ ╚══════╝



-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - Maintenance 

<br>

Add some tasks in our postgresql replication role

```
postgresql_replication_wal_g_cron_backup: "0 1 * * *"
postgresql_replication_wal_g_cron_clean: "0 2 * * *"
postgresql_replication_wal_g_cron_retains: 7
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - Maintenance 

<br>

In the wal-g part - add directories

```
- name: install directories
  file:
    path: "{{ item.dir }}"
    state: directory
    owner: root
    group: root
    mode: "{{ item.mode }}"
  loop:
    - { dir: "/opt/scripts/backups", mode: "0750" }
    - { dir: "/var/log/backups/", mode: "0755" }
    - { dir: "/var/lib/cron/", mode: "0755" }
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - Maintenance 

<br>

In the wal-g part - add pg_basebackup script

```
- name: push wal_pg_basebackup.sh
  template:
    src: wal_pg_basebackup.sh.j2
    dest: /opt/scripts/backups/wal_pg_basebackup.sh
    mode: 750
    owner: postgres
    group: postgres
  when: postgresql_replication_wal_g_enabled
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - Maintenance 

<br>

In the wal-g part - add cron

```
- name: add a cron for backups
  template:
    src: wal_g_backup.j2
    dest: /etc/cron.d/wal_g_backup
    owner: root
    group: root
    mode: 0750
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - Maintenance 

<br>

Add cron template file

```
## Edited by ansible

## Backups
{{ postgresql_replication_wal_g_cron_backup }} root touch /var/lib/cron/start_walgbasebackup_backup.txt && /opt/scripts/backups/wal_pg_basebackup.sh -m && touch /var/lib/cron/end_walgbasebackup_backup.txt

## Cleans
{{ postgresql_replication_wal_g_cron_clean }} root touch /var/lib/cron/start_walgbasebackup_clean.txt && /opt/scripts/backups/wal_pg_basebackup.sh -c {{ postgresql_replication_wal_g_cron_retains }} && touch /var/lib/cron/end_walgbasebackup_clean.txt
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - Maintenance 

<br>

Add to playbook

```
  roles:
    - databases/postgresql/postgresql_replication
    - consul/consul_services
    - monitoring/custom_metrics
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - Maintenance 

<br>

Add our script file


-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - Maintenance 

<br>

Test

```
su postgres -c "psql -c 'CREATE DATABASE test;'"
su postgres -c "psql -d test -c 'CREATE TABLE posts (ts timestamp);'"
su postgres -c "psql -d test -c 'INSERT INTO posts (ts) VALUES (CURRENT_TIMESTAMP);'"
su postgres -c "psql -d test -c 'SELECT * FROM posts;'"
su postgres -c "psql -d test -c 'SELECT pg_switch_wal();'"
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - Maintenance 

<br>

Test

```
su postgres -c "PGHOST=/var/run/postgresql PGUSER=postgres /usr/local/bin/wal-g --config /etc/wal-g/walg.json backup-push /data/postgresql
su postgres -c "psql -d test -c 'INSERT INTO posts (ts) VALUES (CURRENT_TIMESTAMP);'"
su postgres -c "psql -d test -c 'INSERT INTO posts (ts) VALUES (CURRENT_TIMESTAMP);'"
...
su postgres -c "psql -d test -c 'SELECT * FROM posts;'"
su postgres -c "psql -d test -c 'SELECT pg_switch_wal();'"
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - Maintenance 

<br>

Test

```
systemctl stop postgresql # on all instance
rm -rf /data/postgresql/*
su postgres -c "PGHOST=/var/run/postgresql PGUSER=postgres /usr/local/bin/wal-g --config /etc/wal-g/walg.json backup-fetch /data/postgresql LATEST
touch /var/lib/postgresql/16/main/recovery.signal
echo "recovery_target_time = '2024-04-19 06:43:42'" >> /etc/postgresql/16/main/conf.d/replication.conf
systemctl start postgresql

su postgres -c "repmgr status show"
su postgres -c "psql -d test -c 'select pg_promote();'"

./pg_sync_manage.sh -r # on secondary
```