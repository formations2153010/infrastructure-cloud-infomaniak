%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics - Backups consul


Example : consul backups on Object Storage

<br>

Object Storage : Containers/buckets

    * store each file as an object 

    * use metadatas to organize them

    * ideal for large amount of datas (infinity)

    * lower costs

    * lower speed

Different from block storage (disks for instance)

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics - Backups consul


Example : consul backups on Object Storage

<br>

Project :

    * storage consul datas /data/var/lib/consul

    * in a container

    * with restic : deduplication, organization, retention...

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics - Backups consul


Create a dedicated user

```
openstack ec2 credentials list
openstack ec2 credentials create
```

Can use aws s3api

```
vim ~/.aws/credentials
```

```
https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
aws --endpoint-url=https://s3.pub1.infomaniak.cloud s3api list-buckets
aws --endpoint-url=https://s3.pub1.infomaniak.cloud s3api create-bucket --bucket backups_consul
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics - Backups consul

Create a container with terraform

```
resource "openstack_objectstorage_container_v1" "backups-consul" {
  name   = "backups-consul"
  metadata = {
    backups = "consul"
  }
  versioning   = false
}
```


-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics - Backups consul

Restic test

```
export AWS_DEFAULT_REGION="us-east-1"
export RESTIC_REPOSITORY="s3:https://s3.pub1.infomaniak.cloud/backups-consul"
export AWS_ACCESS_KEY_ID=""
export AWS_SECRET_ACCESS_KEY=""
export RESTIC_PASSWORD="6yhaX3cVKJRb/4ad1p3KshRQtWbJLGbtN5ij"
```

```
sudo apt install restic
restic init
restic backup test/
source /root/.config/restic/restic_password_consul.creds
/usr/local/bin/consul snapshot save /tmp/consul-backup.bck
restic backup /tmp/consul-backup.bck
restic restore b4412403 --target /tmp/ --host consul3
# sudo -u restic /home/restic/bin/restic --exclude={/dev,/media,/mnt,/proc,/run,/sys,/tmp,/var/tmp} -r /tmp backup /
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics

Restic script

```
#!/bin/bash
set -euxo pipefail
source /root/.config/restic/restic_password_consul.creds
/usr/local/bin/consul snapshot save /tmp/consul-backup.bck
restic backup /tmp/consul-backup.bck
restic forget \ 
    --keep-last=14 \
    --keep-within-daily=30d \
    --keep-within-weekly=90d \
    --keep-within-monthly 365d \
    --group-by '' \
    --prune \
    --host consul3
```
