%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗███████╗██╗   ██╗ ██████╗██╗      ██████╗  █████╗ ██╗  ██╗
██║ ██╔╝██╔════╝╚██╗ ██╔╝██╔════╝██║     ██╔═══██╗██╔══██╗██║ ██╔╝
█████╔╝ █████╗   ╚████╔╝ ██║     ██║     ██║   ██║███████║█████╔╝ 
██╔═██╗ ██╔══╝    ╚██╔╝  ██║     ██║     ██║   ██║██╔══██║██╔═██╗ 
██║  ██╗███████╗   ██║   ╚██████╗███████╗╚██████╔╝██║  ██║██║  ██╗
╚═╝  ╚═╝╚══════╝   ╚═╝    ╚═════╝╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝



-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Purpose : https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/keycloak/

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

4- create user

5- map role with user

But little fix

```
- name: wait for service up
  uri:
    url: "{{ keycloak_url }}"
    status_code: [200,301,302]
    validate_certs: false
  register: __result
  until: __result.status in [200,301,302]
  retries: 120
  delay: 2
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Create client loop and users loop

```
...
- name: manage keycloak users
  include_tasks: users_management.yml
  loop: "{{ keycloak_users.keys() | list }}"

- name: manage keycloak users roles mapping
  include_tasks: users_mapping_management.yml
  loop: "{{ keycloak_role_mapping.keys() | list }}"
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Create client roles - in previous clien file

```
- name: Create Keycloak Clients Roles
  become: false
  local_action:
    module: community.general.keycloak_role
    auth_client_id: admin-cli
    auth_keycloak_url: "{{ keycloak_url }}"
    auth_realm: master
    auth_username: "{{ keycloak_admin_user }}"
    auth_password: "{{ keycloak_admin_user_password }}"
    validate_certs: false
    state: present
    realm: '{{ keycloak_clients[item]["realm"] }}'
    client_id: '{{ keycloak_clients[item]["client_id"] }}'
    name: "{{ role }}"
  loop: '{{ keycloak_clients[item]["client_roles"] }}'
  loop_control:
    loop_var: role
  run_once: true
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Create our grafana user

```
- name: Create our users
  become: false
  local_action:
    module: community.general.keycloak_user
    auth_client_id: admin-cli
    auth_keycloak_url: "{{ keycloak_url }}"
    auth_realm: master
    auth_username: "{{ keycloak_admin_user }}"
    auth_password: "{{ keycloak_admin_user_password }}"
    validate_certs: false
    state: present
    realm: '{{ keycloak_users[item]["realm"] }}'
    username: '{{ keycloak_users[item]["username"] }}'
    firstName: '{{ keycloak_users[item]["firstname"] }}'
    lastName: '{{ keycloak_users[item]["lastname"] }}'
    email: '{{ keycloak_users[item]["email"] }}'
    enabled: true
    emailVerified: true
    credentials:
      - type: password
        value: '{{ keycloak_users[item]["password"] }}'
        temporary: false # change it
  run_once: true
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Our new playbook

```
- name: Map role to default user
  become: false
  local_action:
    module: community.general.keycloak_user_rolemapping
    auth_client_id: admin-cli
    auth_keycloak_url: "{{ keycloak_url }}"
    auth_realm: master
    auth_username: "{{ keycloak_admin_user }}"
    auth_password: "{{ keycloak_admin_user_password }}"
    validate_certs: false
    state: present
    client_id: '{{ keycloak_role_mapping[item]["client"] }}'
    realm: '{{ keycloak_role_mapping[item]["realm"] }}'
    target_username: '{{ keycloak_role_mapping[item]["username"] }}'
    roles: '{{ keycloak_role_mapping[item]["roles"] }}'
  run_once: true
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Our new playbook

```
- name: install our keycloak
  hosts: meta-app_keycloak
  roles:
    - keycloak_management
  vars:
...
    keycloak_users:
      xpestel:
        realm: "{{ all_keycloak_realm_infrastructure }}"
        username: xpestel
        firstname: xavier
        lastname: pestel
        email: x@moi.fr
        password: password
    keycloak_role_mapping:
      xpestel_to_grafana:
        realm: "{{ all_keycloak_realm_infrastructure }}"
        client: "{{ all_keycloak_client_id }}"
        username: xpestel
        roles:
          - name: editor
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

In all file

```
  all_keycloak_realm_infrastructure: grafana
  all_keycloak_client_id: grafana
  all_keycloak_client_secret: mysecret
  all_keycloak_client_url: g.xavki.fr
  all_keycloak_url: https://k.xavki.fr
```