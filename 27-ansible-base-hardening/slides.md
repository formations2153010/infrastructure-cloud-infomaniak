%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Integration, base and hardening

<br>

Purpose :

  * create a base role

  * add a dedicated playbook and add hardening

  * integrate ansible into terraform

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Integration, base and hardening

<br>

Create ansible role, common to all servers

```
ansible-galaxy init roles/base
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Integration, base and hardening

<br>

Add tasks

```
- name: remove debian user
  ansible.builtin.user:
    name: debian
    state: absent
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Integration, base and hardening

<br>

Add tasks

```
- name: install utils
  ansible.builtin.apt:
    name: curl,wget,unzip,net-tools,dnsutils
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Integration, base and hardening

<br>

Add tasks

```
- name: install rkhunter & clamav
  ansible.builtin.apt: 
    name: rkhunter,clamav
    state: latest
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Integration, base and hardening

<br>

Install dedicated collection for hardening

```
ansible-galaxy collection install devsec.hardening
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Integration, base and hardening

<br>

Add playbook

```
- name: base
  hosts: all,!openvpn1
  become: true
  vars:
    sysctl_overwrite:
      net.ipv4.ip_forward: 1
    ssh_banner: true
    ssh_print_motd: true
    ssh_print_last_log: true
    sftp_enabled: true
    ssh_permit_tunnel: false
    ssh_allow_tcp_forwarding: 'yes'
  pre_tasks:
    - name: Waiting compute...
      wait_for_connection:
        delay: 2
        timeout: 300
  collections:
    - devsec.hardening
  roles:
    - dns-common
    - base
    - os_hardening
    - ssh_hardening
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Integration, base and hardening

<br>

Add ansible to terraform

```
resource "null_resource" "ansible_infrastructure" {
  triggers = {
    always_run = timestamp()
  }
  provisioner "local-exec" {
    command = <<-EOT
      sleep 20s;
      ANSIBLE_CONFIG=../../ansible/ansible.cfg ansible-playbook -u xavki -i ../../ansible/openstack.yml --private-key ~/.ssh/info ../../ansible/infrastructure_base.yml;
      ANSIBLE_CONFIG=../../ansible/ansible.cfg ansible-playbook -u xavki -i ../../ansible/openstack.yml --private-key ~/.ssh/info ../../ansible/infrastructure_consul.yml;
    EOT
  }
    depends_on = [module.consul]
}
```