%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

openvpn

    server ( CA + cert + network )  >  client ( cert + interface + routes )

<br>

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

Installation Openvpn Server step by step

  * installation packages

  * generate a CA (cert/key)

  * create server cert/key

  * build certificate revocate list (crl)

  * generate tls auth key/cert

  * configure the server

  * add iptables rules and persist its

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

Update and install packages

```
- name: Update apt packages
  apt:
    upgrade: "True"
    cache_valid_time: 3600
    
- name: Install openvpn
  apt:
    name: "openvpn,easy-rsa,iptables-persistent"
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

Check if CA already install

```
- name: check if ca dir already.stat.exists
  stat:
    path: "{{ ansible_env.HOME }}/openvpn-ca/"
  register: __check_openvpn_ca_dir_present
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

Create CA dir if not exists

```
- name: "Create CA dir"
  command: make-cadir {{ ansible_env.HOME }}/openvpn-ca
  when: __check_openvpn_ca_dir_present.stat.exists == false
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

Define CA variables (group or host vars)

```
vpn_server_name: "infovpn"
vpn_key_name: "serverVPN"
vpn_key_country: "EU"
vpn_key_province: "FR"
vpn_key_city: "Paris"
vpn_key_org: "Xavki"
vpn_key_email: "xavki@moi.fr"
vpn_key_ou: "xav"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

Set the configuration file to generate CA

```
- name: Customize CA variable configuration
  lineinfile:
    dest: "{{ ansible_env.HOME }}/openvpn-ca/vars"
    regexp: "^{{ item.property | regex_escape() }}="
    line: "{{ item.property }}={{ item.value }}"
  with_items:
    - { property: 'export KEY_NAME', value: '{{ vpn_key_name }}' }
    - { property: 'export KEY_COUNTRY', value: '{{ vpn_key_country }}' }
    - { property: 'export KEY_PROVINCE', value: '{{ vpn_key_province }}' }
    - { property: 'export KEY_CITY', value: '{{ vpn_key_city }}' }
    - { property: 'export KEY_ORG', value: '{{ vpn_key_org }}' }
    - { property: 'export KEY_EMAIL', value: '{{ vpn_key_email }}' }
    - { property: 'export KEY_OU', value: '{{ vpn_key_ou }}' }
    - { property: 'export KEY_CONFIG', value: '{{ ansible_env.HOME }}/openvpn-ca/openssl-1.1.1.cnf' }
    - { property: 'export KEY_DIR', value: '{{ ansible_env.HOME }}/openvpn-ca/keys' }
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

Set the configuration file to generate CA

```
- name: check if ca file already.stat.exists
  stat:
    path: "{{ ansible_env.HOME }}/openvpn-ca/pki/ca.crt"
  register: __check_openvpn_ca_crt_file_present
```

<br>

Then build CA

```
- name: build the certificate authority
  shell: >
    ./easyrsa init-pki;
    ./easyrsa --batch --req-cn="cn_{{ vpn_server_name }}" build-ca nopass;
  args: 
    chdir: "{{ ansible_env.HOME }}/openvpn-ca/"
    executable: /bin/bash
  when: __check_openvpn_ca_crt_file_present.stat.exists == false
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

Check if we already have a crl pem file

```
- name: check if crl server file already.stat.exists
  stat:
    path: "{{ ansible_env.HOME }}/openvpn-ca/pki/crl.pem"
  register: __check_openvpn_crl_server_crt_file_present
```

<br>

If not

```
- name: build crl and server certificate
  shell: >
    ./easyrsa --batch build-server-full "server_{{ vpn_server_name }}" nopass;
    EASYRSA_CRL_DAYS=3650 ./easyrsa gen-crl;
  args: 
    chdir: "{{ ansible_env.HOME }}/openvpn-ca/"
    executable: /bin/bash
  when: __check_openvpn_crl_server_crt_file_present.stat.exists == false
```
