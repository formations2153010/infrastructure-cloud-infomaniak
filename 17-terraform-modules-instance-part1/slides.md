%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance

We have a problem ??

<br>

For one instance, as we see :

    * 1 data source : openstack_networking_subnet_ids_v2

    * 1 resource : openstack_networking_floatingip_v2

    * 1 resource : openstack_compute_instance_v2

    * 1 resource : openstack_compute_floatingip_associate_v2

    * x resources : volumes, ports...

<br>

We need to refactor it !!!

  * solution 1 resource to manage them all = module (like a role in ansible)


-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance

<br>

What will we need to have in instance module ?
    (many tutos dedicated to instance module)

    * sometimes a fixed internal ip

    * sometimes a fixed floating ip

    * sometimes a pool of servers

    * sometimes one or many volumes (with fixed device name)

    * always a standard installation (change to a specific user with our ssh key)

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance

<br>

Module Variables : variables.tf

```
variable "instance_network_external_id" {
  type    = string
}
variable "instance_network_external_name" {
  type    = string
}
variable "instance_name" {
  type = string
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance

<br>

Module Variables : variables.tf

```
variable "instance_image_id" {
  type = string
  default = "cdf81c97-4873-473b-b0a3-f407ce837255"
}
variable "instance_flavor_name" {
  type    = string
  default = "a1-ram2-disk20-perf1"
}
variable "instance_security_groups" {
  type    = list(any)
  default = ["default"]
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance

<br>

Module Variables : variables.tf

```
variable "instance_key_pair" {
  type = string
}
variable "metadatas" {
  type = map(string)
  default = {
    "environment" = "dev"
  }
}
variable "instance_network_internal" {
  type    = string
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance

<br>

Module core : instance.tf

```
data "openstack_networking_subnet_ids_v2" "ext_subnets" {
  network_id = var.network_external_id
}
```

```
resource "openstack_networking_floatingip_v2" "floatip_1" {
  pool       = var.instance_network_external_name
  subnet_ids = data.openstack_networking_subnet_ids_v2.ext_subnets.ids
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance

<br>

Module core : instance.tf

```
resource "openstack_compute_instance_v2" "openvpn" {
  name            = var.instance_name
  image_id        = var.instance_image_id
  flavor_name     = var.instance_flavor_name
  metadata        = var.metadatas
  security_groups = var.instance_security_groups
  key_pair        = var.instance_key_pair
  network {
    name = var.instance_network_internal
  }
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance

<br>

Module core : instance.tf

```
resource "openstack_compute_floatingip_associate_v2" "fip_assoc" {
  floating_ip = openstack_networking_floatingip_v2.floatip_1.address
  instance_id = openstack_compute_instance_v2.openvpn.id
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance

<br>

Module output : outputs.tf

```
output "instance_internal_ip" {
  description = "Internal"
  value       = try(openstack_compute_instance_v2.instance.network[0].fixed_ip_v4,null)
}
output "instance_external_ip" {
  description = "Public"
  value       = try(openstack_networking_floatingip_v2.floatip_1[0].address,null)
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance

<br>

Module usage

```
module "openvpn" {
  source                      = "../modules/instances"
  instance_name               = "openvpn"
  instance_key_pair           = openstack_compute_keypair_v2.ssh_public_key.name
  instance_security_groups    = [openstack_networking_secgroup_v2.openvpn.name, openstack_networking_secgroup_v2.ssh.name, "default"]
  instance_network_internal   = "internal_dev"
  instance_network_external_name = "ext-floating1"
  instance_network_external_id  = "0f9c3806-bd21-490f-918d-4a6d1c648489"
  metadatas                   = {
    environment          = "dev"
  }
  depends_on = [openstack_networking_subnet_v2.network_subnet,openstack_networking_secgroup_rule_v2.secgroup_openvpn_rule_v4]
}
```

Be careful depends_on must be add to module
