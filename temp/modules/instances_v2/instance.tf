data "template_file" "userdata" {
  template = <<CLOUDCONFIG
#cloud-config
bootcmd:
  - printf "[Resolve]\nDNS=8.8.8.8 1.1.1.1\n" > /etc/systemd/resolved.conf
  - [systemctl, restart, systemd-resolved]
users:
  - default
  - name: xavki
    primary_group: xavki
    groups: [sudo,adm]
    sudo: ALL=(ALL) NOPASSWD:ALL
    groups: sudo
    shell: /bin/bash
    ssh-authorized-keys: 
    - ${var.instance_ssh_key}
    - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDq8OzUqQ27Ifs+H/jLvsj8M68tCG7mtoaGBDBzy7mKc8cQQ2FKKYH3YUM6qi9HWcFU8YEaI7oSvfnvXG5q/sNSJCH5GOy2GDqqUqFsVAeyco11wDr3OKBRCpWeeuTvFRPto15RbnO0Icyyhxa2UveDtdZkK+wiJsa34zE+h8dMRdWn4BBYRVuFufIEmg/pOWff/CVH26lr5hWG2yO7f6esqMgdA1Do5dAQuUic+tOcN003jvM+Fd+IDYpTAqOTgpYMdpo6YRkUhfPzDlylNp1/DBHryfqc3ypeYP4OoJBTig91zm2BUaz0Dx8wUkFB/V02xOE/xfJSsaGyAab7aeim8CLX8TT544Z7lif+M0t/UYM4EPAOEARW75Mbe4NUrmB26MdJAfZncaS0U+qF5RBGVCpNLFmXyChmzyyyvJhtJGCAWFZwKEIRl+SgEASlBq4G9mGwE9fv0X4WPxBXF1T/I2aq8nxNjLfMfjgmTWM1wGNRc4fGeT2fg25a0HsK6wk= djimi@djimis-MacBook-Pro.local"
CLOUDCONFIG
}

data "openstack_networking_network_v2" "network" {
  name = var.instance_network_name
}

data "openstack_networking_subnet_v2" "subnet" {
  name = var.instance_network_name
}

resource "openstack_networking_port_v2" "internal_port" {
  count      = var.instance_count
  name       = "internal-${var.instance_name}${count.index + 1}"
  network_id = data.openstack_networking_network_v2.network.id
  port_security_enabled = false
  fixed_ip   {
    subnet_id = data.openstack_networking_subnet_v2.subnet.id 
    ip_address = var.instance_internal_fixed_ip == "" ? "" : "${var.instance_internal_fixed_ip}${count.index + 1}"
  }
  #dynamic "allowed_address_pairs" {
  #  for_each = length(var.allowed_addresses) == 0 ? [] : var.allowed_addresses
  #  content {
  #    ip_address = allowed_address_pairs.value
  #  }
  #}
}

data openstack_networking_port_v2 "port" {
  count      = var.instance_count
  name       = "internal-${var.instance_name}${count.index + 1}"

  depends_on = [ openstack_networking_port_v2.internal_port ]
}

resource "openstack_compute_instance_v2" "instance" {
  count           = var.instance_count
  name            = "${var.instance_name}${count.index + 1}"
  image_id        = var.instance_image_id
  flavor_name     = var.instance_flavor_name
  metadata        = var.metadatas
  #security_groups = var.instance_security_groups
  user_data = data.template_file.userdata.rendered

  network {
    port = data.openstack_networking_port_v2.port[count.index].id
  }

  depends_on = [ openstack_networking_port_v2.internal_port ]
}

data "openstack_networking_network_v2" "ext_network" {
  count = var.public_floating_ip ? 1 : 0
  name = "ext-floating1"
}

data "openstack_networking_subnet_ids_v2" "ext_subnets" {
  count = var.public_floating_ip ? 1 : 0
  network_id = data.openstack_networking_network_v2.ext_network[count.index].id
}

resource "openstack_networking_floatingip_v2" "floatip_1" {
  count = var.public_floating_ip  && var.public_floating_ip_fixed == "" ? 1 : 0
  pool       = data.openstack_networking_network_v2.ext_network[count.index].name
  subnet_ids = data.openstack_networking_subnet_ids_v2.ext_subnets[count.index].ids
}

resource "openstack_compute_floatingip_associate_v2" "fip_assoc" {
  count = var.public_floating_ip ? 1 : 0
  floating_ip = var.public_floating_ip_fixed != "" ? var.public_floating_ip_fixed : openstack_networking_floatingip_v2.floatip_1[count.index].address
  instance_id = openstack_compute_instance_v2.instance[count.index].id
}

locals {
  device_names = ["/dev/sdb","/dev/sdc","/dev/sdd","/dev/sde","/dev/sdf","/dev/sdg"]
}

locals {
  instance_volume_map =  merge([
    
    for idxi, instance in openstack_compute_instance_v2.instance.*:
    {
      for idxv in range(var.instance_volumes_count):      
        "${instance.name}-volume-${idxv}" => {
            instance_name     = instance.name
            instance_id       = instance.id
            volume_name       = "${instance.name}-volume${idxv}"
            device            = local.device_names[idxv]
          }
    }
  ]...)
}
