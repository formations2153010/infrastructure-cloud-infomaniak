%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Multi external IPs & Cloud Init

Improvement for external ip (ips) : manage instance group


-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Multi external IPs & Cloud Init


<br>

Change name and use the instance count variable

```
resource "openstack_networking_floatingip_v2" "floatip_1_random" {
  count = var.public_floating_ip  && var.public_floating_ip_fixed == "" ? var.instance_count : 0
  pool       = var.instance_network_external_name
  subnet_ids = data.openstack_networking_subnet_ids_v2.ext_subnets[0].ids
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Multi external IPs & Cloud Init


<br>

Change name and use a counter based on instance_count 

And split random vs fixed floating ip

```
resource "openstack_compute_floatingip_associate_v2" "fip_assoc_random" {
  count = var.public_floating_ip && var.public_floating_ip_fixed == ""? var.instance_count : 0
  floating_ip = openstack_networking_floatingip_v2.floatip_1_random[count.index].address
  instance_id = openstack_compute_instance_v2.instance[count.index].id
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Multi external IPs & Cloud Init


<br>

Change name and use a counter based on instance_count 

```
resource "openstack_compute_floatingip_associate_v2" "fip_assoc_fixed" {
  count = var.public_floating_ip_fixed != "" ? 1 : 0
  floating_ip = var.public_floating_ip_fixed
  instance_id = openstack_compute_instance_v2.instance[count.index].id
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Multi external IPs & Cloud Init


<br>

Split the output and use wildcard

```
output "instance_internal_ip" {
  description = "Internal"
  value       = try(openstack_compute_instance_v2.instance[*].network[0].fixed_ip_v4,null)
}
output "instance_external_ip_random" {
  description = "Public"
  value       = try(openstack_networking_floatingip_v2.floatip_1_random[*].address,null)
}
output "instance_external_ip_fixed" {
  description = "Public"
  value       = try(var.public_floating_ip_fixed,null)
}
```

Note : in ansible usage > module.openvpn.instance_external_ip_random

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Multi external IPs & Cloud Init

Documentation :

https://cloudinit.readthedocs.io/en/latest/reference/examples.html

https://cloudinit.readthedocs.io/en/latest/reference/network-config-format-v2.html

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Multi external IPs & Cloud Init


<br>

An example of template file

```
data "template_file" "init" {
  template = "${file("${path.module}/init.tpl")}"
  vars = {
    consul_address = "${aws_instance.consul.private_ip}"
  }
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Multi external IPs & Cloud Init

<br>

cloud init into a template block

```
data "template_file" "userdata" {
  template = <<CLOUDCONFIG
#cloud-config
bootcmd:
  - printf "[Resolve]\nDNS=8.8.8.8 1.1.1.1\n" > /etc/systemd/resolved.conf
  - [systemctl, restart, systemd-resolved]
users:
  - default
  - name: xavki
    primary_group: xavki
    groups: [sudo,adm]
    sudo: ALL=(ALL) NOPASSWD:ALL
    groups: sudo
    shell: /bin/bash
    lock_passwd: true
    ssh-authorized-keys: 
    - ${var.instance_ssh_key}
CLOUDCONFIG
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module Instance - Multi external IPs & Cloud Init


<br>

Add it into instance resource

```
  user_data = data.template_file.userdata.rendered
```

Add in variable

```
variable "instance_ssh_key" {
  type = string
}
```

Add in module usage

```
instance_ssh_key               = var.ssh_public_key_default_user
```

