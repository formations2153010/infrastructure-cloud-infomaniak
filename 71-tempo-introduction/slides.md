%title: Infra Cloud Infomaniak
%author: xavki


████████╗███████╗███╗   ███╗██████╗  ██████╗ 
╚══██╔══╝██╔════╝████╗ ████║██╔══██╗██╔═══██╗
   ██║   █████╗  ██╔████╔██║██████╔╝██║   ██║
   ██║   ██╔══╝  ██║╚██╔╝██║██╔═══╝ ██║   ██║
   ██║   ███████╗██║ ╚═╝ ██║██║     ╚██████╔╝
   ╚═╝   ╚══════╝╚═╝     ╚═╝╚═╝      ╚═════╝ 


-----------------------------------------------------------------------------------------------------------

# Tempo : Introduction

<br>

What is it ?

  * traces : transactions & requests into microservices, services and infrastructures
  
  * tempo : a backend to store all your traces

  * traces = many spans 

  * traces components : services, operations, trace id, timestamp, duration, parent id

  * generate metrics with traces

  * 2 representations : node graph & flame graph

  * no direct search architecture : pass by metrics or logs before


-----------------------------------------------------------------------------------------------------------

# Tempo : Introduction

<br>


Github : https://github.com/grafana/tempo
Official website : https://grafana.com/docs/tempo/latest/

Language : Golang

-----------------------------------------------------------------------------------------------------------

# Tempo : Introduction

<br>

Features : 

    * local and s3 storage

    * generate metrics

    * no additional backend (as jaeger)

    * can receive multiple standards : jaeger (deprecated), zipkin, opentelemetry

    * TraceQL language

    * examplars : to link with logs or metrics

-----------------------------------------------------------------------------------------------------------

# Tempo : Introduction

<br>

Like Loki :

    * 2 modes : monolithe & microservices

    * server

<br>

Write :

    * distributor : receive traces in multiple formats

    * ingester : batch traces in blocks to ingest them (wal before S3)

    * metrics-generator : generate metrics and push its to remote_write

    * storage

<br>

Read :

    * query-frontend : receive queries and split its

    * querier : find responds to requests

    * compactor : reduce size of blocks

    (ingester if contain responses)

-----------------------------------------------------------------------------------------------------------

# Tempo : Introduction

<br>

Intermediare pipeline :  Otel or Grafana Agent

  * to do sampling : 
      * tail based sampling (ex: 10% on streaming)
      * vs head based sampling : waiting the end of trace to chosse what to do

  * to batch trace

  * to filter on error traces
